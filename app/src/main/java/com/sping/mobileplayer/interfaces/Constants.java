package com.sping.mobileplayer.interfaces;

/**
 * 常量
 * 
 * @author SONGPING
 * @date 2018-7-26
 */
public interface Constants {
	/** 一秒钟对应的毫秒值 */
	long SECOND_MILLIS = 1000;
	/** 一分钟对应的毫秒值 */
	long MINUTE_MILLIS = 60 * SECOND_MILLIS;
	/** 一小时对应的毫秒值 */
	long HOUR_MILLIS = 60 * MINUTE_MILLIS;
}
