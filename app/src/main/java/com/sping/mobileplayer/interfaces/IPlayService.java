package com.sping.mobileplayer.interfaces;

import java.util.ArrayList;

import com.sping.mobileplayer.bean.AudioItem;

/**
 * 音乐播放服务接口
 * 
 * @author PING
 * @date 2018-8-19
 */
public interface IPlayService {
	/** 播放 */
	void start();

	/** 暂停 */
	void pause();

	/** 上一首 */
	void pre();

	/** 下一首 */
	void next();

	/** 打开一个音频 */
	void openAudio();

	/** 是否正在播放 */
	boolean isPlaying();

	/** 获取当前播放位置 */
	int getCurrentPosition();

	/** 获取音频总时长 */
	int getDuration();

	/** 跳转 */
	void seekTo(int msec);

	/**
	 * 切换播放模式
	 * @return 返回切换后的播放模式
	 */
	int switchPlayMode();

	/** 获取当前播放模式 */
	int getCurrentPlayMode();

	/** 获取当前播放的音频在列表中的索引 */
	int getCurrentPlayIndex();
	
	/** 获取当前播放的音频JavaBean */
	AudioItem getCurrentAudioItem();
	
	/** 获取当音频JavaBeanList */
	ArrayList<AudioItem> getAudioItemList();

	/** 更新歌单数据 */
	void updatePlayList(ArrayList<AudioItem> audios);
	
	/** 更新播放歌单索引 */
	void updatePlayList(int currentPlayIndex);
}
