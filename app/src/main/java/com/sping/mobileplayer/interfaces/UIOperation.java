package com.sping.mobileplayer.interfaces;

import android.view.View;
import android.view.View.OnClickListener;

/**
 * UI操作接口
 * 
 * @author PING
 * @date 2018-7-24
 */
public interface UIOperation extends OnClickListener {
	/** 返回一个用于显示界面的布局ID */
	int getLayoutResID();

	/** 初始化View */
	void initView();

	/** 初始化监听器 */
	void initListener();

	/** 初始化数据,并显示到界面上 */
	void initData();

	/**
	 * 单击事件在这个方法中处理
	 * 
	 * @param v
	 *            单击的控件
	 * @param id
	 *            单击控件的id
	 */
	void onClick(View v, int id);
}
