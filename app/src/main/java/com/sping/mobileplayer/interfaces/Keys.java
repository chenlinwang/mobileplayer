package com.sping.mobileplayer.interfaces;

public interface Keys {

	String ITEMS = "items";
	String CURRENT_POSITION_IN_LIST = "currentPositionInList";
	String ITEM = "item";
	String CURRENT_PLAY_MODE = "currentPlayMode";
	String WHAT = "what";
	String IS_FROM__AUDIO_PLAYER = "isFromAudioPlayer";

}
