package com.sping.mobileplayer.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.sping.mobileplayer.R;
import com.sping.mobileplayer.adapter.MainAdapter;
import com.sping.mobileplayer.fragment.AudioFragment;
import com.sping.mobileplayer.fragment.VideoFragment;
import com.sping.mobileplayer.interfaces.Keys;
import com.sping.mobileplayer.util.Utils;

public class MainActivity extends BaseActivity {

    private TextView tv_video;
    private TextView tv_audio;
    private View view_indicator;
    private ViewPager view_pager;
    private int indicatorWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //解决按home，再次点击图标重新启动的问题 !=0 已经存在任务栈, == 0 不存在Activity实例
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) == 0) {
           // finish();
           // return;
            enterSplash();
        }
        super.onCreate(savedInstanceState);

    }

    /**
     * 进入闪屏页
     */
    protected void enterSplash() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
    }

    @Override
    public int getLayoutResID() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        tv_video = findView(R.id.tv_video);
        tv_audio = findView(R.id.tv_audio);
        view_indicator = findView(R.id.view_indicator);
        view_pager = findView(R.id.view_pager);
        initIndicator();
    }

    /**
     * 初始化指示器
     */
    private void initIndicator() {
        int screenWidth = Utils.getScreenWidth(this);
        indicatorWidth = screenWidth / 2;
        view_indicator.getLayoutParams().width = indicatorWidth;
        view_indicator.requestLayout();//通知这个view去更新它的布局参数
    }

    @Override
    public void initListener() {
        tv_video.setOnClickListener(this);
        tv_audio.setOnClickListener(this);
        view_pager.setOnPageChangeListener(new OnPageChangeListener() {

            // 当页面被选择的时候
            @Override
            public void onPageSelected(int position) {
                changeTitleTextState(position == 0);
            }

            // 当页面滚动的时候
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                scrollIndicator(position, positionOffset);
            }

            // 当页面滚动状态发生改变的时候
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * 滚动指示线
     *
     * @param position
     * @param positionOffset
     */
    protected void scrollIndicator(int position, float positionOffset) {
        float translationX = indicatorWidth * position + indicatorWidth * positionOffset;
        ViewHelper.setTranslationX(view_indicator, translationX);
    }

    @Override
    public void initData() {
        // 默认选中视频
        changeTitleTextState(true);
        // 初始化ViewPager
        initViewPager();
    }

    /**
     * 初始化ViewPager
     */
    private void initViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(new AudioFragment());//
        fragments.add(new VideoFragment());
        MainAdapter adapter = new MainAdapter(getSupportFragmentManager(),
                fragments);
        view_pager.setAdapter(adapter);

        //从音乐界面回来,返回到音乐fragment
        if (getIntent().getBooleanExtra(Keys.IS_FROM__AUDIO_PLAYER, false)) {
            view_pager.setCurrentItem(0);//
        }

    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.tv_video:
                view_pager.setCurrentItem(0);
                break;
            case R.id.tv_audio:
                view_pager.setCurrentItem(1);

                break;

            default:
                break;
        }
    }

    /**
     * 改变标题状态
     *
     * @param isSelectVideo 是否选择了视频
     */
    private void changeTitleTextState(boolean isSelectVideo) {
        // 改变文本颜色
        tv_video.setSelected(isSelectVideo);
        tv_audio.setSelected(!isSelectVideo);

        // 改变文本大小
        scaleTitle(isSelectVideo ? 1.3f : 1.0f, tv_video);
        scaleTitle(!isSelectVideo ? 1.3f : 1.0f, tv_audio);
    }

    /**
     * 缩放标题
     *
     * @param scale   缩放的比例
     * @param textViw
     */
    private void scaleTitle(float scale, TextView textViw) {
        ViewPropertyAnimator.animate(textViw).scaleX(scale).scaleY(scale)
                .setDuration(200);
    }

    @Override
    public void onBackPressed() {
//		此方法直接将当前Activity所在的Task移到后台，同时保留activity顺序和状态。
//		this.moveTaskToBack(true);
        super.onBackPressed();
    }
}
