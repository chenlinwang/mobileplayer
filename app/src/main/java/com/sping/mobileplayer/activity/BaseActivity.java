package com.sping.mobileplayer.activity;

import com.sping.mobileplayer.R;
import com.sping.mobileplayer.interfaces.UIOperation;
import com.sping.mobileplayer.util.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * activity 的基类,其他activity应该继承这个类
 * 
 * @author SONGPING
 * @date 2018-7-23
 */
public abstract class BaseActivity extends FragmentActivity implements UIOperation, EasyPermissions.PermissionCallbacks {

	public final static String[] PERMS_READ_WRITE = {Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE};
	public static final int REQUEST_CODE = 1001;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);


		setContentView(getLayoutResID());// 多态
		View rootView = findViewById(android.R.id.content);// android.R.id.content这个id可以获取到activity的根view
		Utils.setButtonOnClickListener(rootView,this);		
		initView();
		initListener();

		if (!checkPermission(this, PERMS_READ_WRITE)) {
			requestPermission(this, "播放媒体需要读写外部存储权限", REQUEST_CODE, PERMS_READ_WRITE);
		}else {
			initData();
		}

	}

	/**
	 * 查找View ,这个方法可以省去我们的强转操作
	 * @param id view id
	 * @return view
	 */
	public <T> T findView(int id) {
		@SuppressWarnings("unchecked")
		T view = (T) findViewById(id);
		return view;

	}


	/**
	 * 在屏幕中间显示一个Toast
	 * @param text msg
	 */
	public void showToast(String text){		
		Utils.showToast(this, text);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:// 处理共同操作
			finish();
			break;

		default:

			// 如果单击的不是返回按钮,则还是由子类去做处理
			onClick(v, v.getId());
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//当从软件设置界面，返回当前程序时候
		//if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {//执行Toast显示或者其他逻辑处理操作
		//
		//}
	}


	/**
	 * 1.检查有没有权限
	 *
	 * @param context return true:已经获取权限
	 *                return false: 未获取权限，主动请求权限
	 */
	public static boolean checkPermission(Activity context, String[] perms) {
		return EasyPermissions.hasPermissions(context, perms);
	}

	/**
	 * 2.请求权限
	 * @param context 上下文
	 * @param tip 请求权限时给用户提示
	 * @param requestCode 请求码
	 * @param perms 权限list
	 */
	public static void requestPermission(Activity context, String tip, int requestCode, String[]
			perms) {
		EasyPermissions.requestPermissions(context, tip, requestCode, perms);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
										   @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		//将请求结果传递EasyPermission库处理
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}

	/**
	 * 请求权限成功。
	 * 可以弹窗显示结果，也可执行具体需要的逻辑操作
	 *
	 * @param requestCode 请求码
	 * @param perms 权限list
	 */
	@Override
	public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
		initData();
		showToast( "授权成功");
	}

	/**
	 * 请求权限失败
	 *
	 * @param requestCode 请求码
	 * @param perms 权限list
	 */
	@Override
	public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
		Utils.showToast(this, "用户授权失败");
		/**
		 * 若是在权限弹窗中，用户勾选了'NEVER ASK AGAIN.'或者'不在提示'，且拒绝权限。
		 * 这时候，需要跳转到设置界面去，让用户手动开启。
		 */
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			new AppSettingsDialog.Builder(this).build().show();
		}
	}


}
