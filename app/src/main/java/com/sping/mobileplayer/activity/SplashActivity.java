package com.sping.mobileplayer.activity;

import com.sping.mobileplayer.R;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

public class SplashActivity extends BaseActivity {
//public class SplashActivity extends Activity {

	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        //解决按home，再次点击图标重新启动的问题
        // if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
        //     finish();
        //     return;
        // }

//        if (!this.isTaskRoot()) {
//            finish();
//            return;
//        }
        super.onCreate(savedInstanceState);
	}

	@Override
	public int getLayoutResID() {
		return R.layout.activity_splash;
	}

	@Override
	public void initView() {

	}

	@Override
	public void initListener() {

	}

	@Override
	public void initData() {

			delayEnterHome();

	}

	/**
	 * 延迟0.5秒进入主页
	 */
	private void delayEnterHome() {
		mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				enterHome();
			}

		}, 2000);
	}

	/**
	 * 进入首页
	 */
	protected void enterHome() {
		// Intent intent = new Intent(this, MainActivity.class);
		// startActivity(intent);
		finish();
	}

	@Override
	public void onClick(View v, int id) {

	}

	@Override
	public void onBackPressed() {
		// super.onBackPressed();
		// 禁止在闪屏页按返回键,避免结束闪屏页后,定时时间到会启动HomeActivity

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mHandler.removeCallbacksAndMessages(null);
			enterHome();
			break;

		default:
			break;
		}

		return super.onTouchEvent(event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacksAndMessages(null);
	}
}
