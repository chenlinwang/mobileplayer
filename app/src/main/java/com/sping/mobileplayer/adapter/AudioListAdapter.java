package com.sping.mobileplayer.adapter;

import com.sping.mobileplayer.R;
import com.sping.mobileplayer.bean.AudioItem;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class AudioListAdapter extends CursorAdapter {

	public AudioListAdapter(Context context, Cursor c) {
		super(context, c);

	}

	// 创建一个View
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// 填充出一个View
		View view = View.inflate(context, R.layout.adapter_audio_list, null);

		// 用一个ViewHolder保存View中的子控件
		ViewHolder holder = new ViewHolder();
		holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
		holder.tv_artist = (TextView) view.findViewById(R.id.tv_artist);

		// 把ViewHolder保存到View中
		view.setTag(holder);

		return view;
	}

	// 把cursor中的数据绑定到View上进行显示
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		AudioItem item = AudioItem.fromCursor(cursor);

		holder.tv_title.setText(item.getTitle());
		holder.tv_artist.setText(item.getArtist());

	}

	class ViewHolder {
		TextView tv_title;
		TextView tv_artist;

	}

}
